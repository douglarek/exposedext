package xin.lingchao.exposedext.sql

import com.fasterxml.jackson.databind.ObjectMapper
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import org.postgresql.util.PGobject
import java.sql.PreparedStatement

/**
 * A jsonb column class for Exposed.
 */
class JsonColumnType : ColumnType() {
    override fun sqlType() = "JSONB"

    override fun setParameter(stmt: PreparedStatement, index: Int, value: Any?) {
        val jsonObject = PGobject()
        jsonObject.type = sqlType()
        jsonObject.value = mapper.writeValueAsString(value ?: "{}")
        super.setParameter(stmt, index, jsonObject)
    }

    override fun valueFromDB(value: Any): Any = when (value) {
        is PGobject -> mapper.readValue(value.value, Map::class.java)
        else -> {
            error("$value cannot be cast to java.util.Map")
        }
    }

    companion object {
        private val mapper = ObjectMapper()
    }
}

/**
 * A Table extension fun for json type.
 *
 * @param name column name in table.
 */
fun Table.json(name: String): Column<Map<*, *>> = registerColumn(name, JsonColumnType())